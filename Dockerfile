FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /RubySpace
WORKDIR /RubySpace
COPY Gemfile /RubySpace/Gemfile
COPY Gemfile.lock /RubySpace/Gemfile.lock
RUN bundle install
COPY . /RubySpace

